import unittest
import pystr

class pystr_test_class(unittest.TestCase):
    def test_is_anagram(self):
        # execute function without printing result:
        self.assertTrue(pystr.is_anagram("string", "string"), True)
        self.assertTrue(pystr.is_anagram("Astronomer", "moon starer"), True)
        self.assertFalse(pystr.is_anagram("rat", "car"), False)
        # execute function with printing result:
        self.assertTrue(pystr.is_anagram("string", "string", True), True)
        self.assertTrue(pystr.is_anagram("Astronomer", "moon starer", True), True)
        self.assertFalse(pystr.is_anagram("rat", "car", True), False)

    def test_process(self):
        self.assertEqual(pystr.process("Str1 ngs"), "gnrsst")
        self.assertEqual(pystr.process("test"), "estt")

if __name__ == '__main__':
    unittest.main()
