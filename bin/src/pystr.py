# pystr_lib.py -- python string libraries

def process(string):
    '''Initially used for testing strings with `is_anagram()` function. Takes a string argument and returns a sorted, cased string with only alphabetical characters included.'''
    string = "".join(sorted(c.lower() for c in string if c.isalpha()))
    return string

def is_anagram(string, test, print_res = False):
    '''is_anagram takes two string arguments, utilizes `process()` and returns an equality boolean indicating whether the two string are examples of anagrams.'''
    sproc, tproc = process(string), process(test)

    if print_res:
        print(f"{test} is an anagram of {string}") if sproc == tproc else print(f"{test} is not an anagram of {string}")

    return sproc == tproc
